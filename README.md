### Automated by
- Name: Pedro Rodríguez
- Email: prodsistemas@gmail.com
- Date of the test: 10/12/2021

### Project Repository - Downloading the Framework

devsavant-test-automation framework source code can be downloaded from:

https://bitbucket.org/prodsistemas/devsavant-test-automation/src/master/


## Pre-requirements
In order to build the project and run the tests, it will be necessary to have installed Java JDK 8 at least and also MAVEN.
Another needed thing is to have the `JAVA_HOME` and `MAVEN_HOME` environment variables properly configured.
If you need more information about how to set `JAVA_HOME` and `MAVEN_HOME`, you can visit the following web page: https://dev.to/vanessa_corredor/instalar-manualmente-maven-en-windows-10-50pb


## Framework configuration files
- serenity.conf         (WebdriverManager settings to download webdriver binaries)
- serenity.properties   (Global execution settings: Implicit waits, explicit waits)



### Executing the tests
To run the devsavant-test-automation project, you can either just run the `CucumberTestSuite` test runner class, or run `mvn clean verify` from the command line.

```json
$ mvn clean verify
```

The test results will be recorded in `target/site/serenity/index.html` path.
