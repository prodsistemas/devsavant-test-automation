package com.devsavant.automation.tasks.locators;

import org.openqa.selenium.By;

public class SearchingLocators {

    private SearchingLocators() {
        throw new IllegalStateException("Searching Locators class");
    }

    public static final By TXT_SEARCH = By.className("gLFyf");
    public static final By BTN_FEEL_WITH_LUCKY = By.className("RNmpXc");

}
