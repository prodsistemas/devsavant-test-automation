package com.devsavant.automation.tasks;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;

public class NavigationTasks extends UIInteractionSteps {

    GoogleHomePage googleHomePage;

    @Step
    public void theGoogleWebPage() {
        googleHomePage.open();
    }
}
