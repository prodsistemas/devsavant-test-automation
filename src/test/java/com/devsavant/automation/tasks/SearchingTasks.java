package com.devsavant.automation.tasks;

import com.devsavant.automation.tasks.locators.SearchingLocators;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class SearchingTasks extends UIInteractionSteps {

    @Step
    public void theWord(String wordToFind) {
        waitFor(ExpectedConditions.elementToBeClickable(SearchingLocators.TXT_SEARCH));
        $(SearchingLocators.TXT_SEARCH).sendKeys(wordToFind);
    }

    @Step
    public void TryFeelWithLucky() {
        waitFor(ExpectedConditions.elementToBeClickable(SearchingLocators.BTN_FEEL_WITH_LUCKY));
        $(SearchingLocators.BTN_FEEL_WITH_LUCKY).click();
    }
}
