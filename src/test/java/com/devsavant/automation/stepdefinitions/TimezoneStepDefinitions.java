package com.devsavant.automation.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;

public class TimezoneStepDefinitions {

    List<String> timeZonesResponse;
    Response response;

    @When("all timezones are requested")
    public void requestAllTimezones() {
        timeZonesResponse = SerenityRest.when()
                .get("http://worldtimeapi.org/api/timezone")
                .then()
                .extract()
                .response()
                .jsonPath()
                .getList("$");
    }

    @Then("the response values should not be empty")
    public void verifyThatTheResponseIsNotEmpty() {
        assertThat(timeZonesResponse).isNotEmpty();
    }

    @Then("the response values should not be null")
    public void verifyThatTheResponseIsNotNull() {
        assertThat(timeZonesResponse).isNotNull();
    }

    @When("the timezone for {string} is requested")
    public void requestTimeZoneForBogota(String timezone) {
        response = SerenityRest.when()
                .get("http://worldtimeapi.org/api/timezone/" + timezone)
                .then()
                .extract()
                .response();
    }

    @Then("the status code should not be {string}")
    public void verifyThatTheStatusCodeIsNot(String status) {
        response.then().statusCode(not(status));
    }

    @Then("the field {string} in the response should not be present")
    public void verifyThatTheResponseDoesNotContainsSpecifiedDataType(String field) {
        response.then()
                .assertThat()
                .body("$", not(hasKey(field)));
    }
}
