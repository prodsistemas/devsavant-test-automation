package com.devsavant.automation.stepdefinitions;

import com.devsavant.automation.tasks.NavigationTasks;
import com.devsavant.automation.tasks.SearchingTasks;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class SearchingStepDefinitions {

    @Steps
    NavigationTasks navigateTo;
    @Steps
    SearchingTasks searchFor;

    @Given("{string} wants to find for a word in Google")
    public void navigateToGoogle(String actor) {
        navigateTo.theGoogleWebPage();
    }

    @When("he/she searches for {string} word")
    public void searchFor(String wordToFind) {
        searchFor.theWord(wordToFind);
    }

    @Then("he/she should be able to access directly in the web page using the feel with lucky option")
    public void verifyThatTheUserCanAccessToTheWebPage() {
        searchFor.TryFeelWithLucky();
        assertThat("The loaded web page is not Devsavant", getDriver().getTitle(), is("DEVSAVANT"));
        Serenity.takeScreenshot();
    }
}
