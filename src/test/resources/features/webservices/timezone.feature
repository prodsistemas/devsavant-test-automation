Feature: Timezone verification
  In order to check the timezone responses
  As a user
  I want to request timezones

  @Test1
  Scenario: Verify that the timezone response does not contain space character
    When all timezones are requested
    Then the response values should not be empty
    And the response values should not be null

  @Test2
  Scenario Outline: Verify that the response for a specific timezone does not contain wrong information
    When the timezone for "<timezone>" is requested
    Then the status code should not be "<status>"
    And the field "<field>" in the response should not be present
    Examples:
      | timezone                | status | field    |
      | America/Bogota          | 400    | error    |
      | America/InvalidTimeZone | 200    | timezone |